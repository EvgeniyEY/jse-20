package ru.ermolaev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.enumeration.Role;


public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String commandName();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

}
