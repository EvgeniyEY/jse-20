package ru.ermolaev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable Integer index = TerminalUtil.nextNumber();
        if (index == null) return;
        index = index - 1;
        @NotNull final Task task = serviceLocator.getTaskService().findTaskByIndex(userId, index);
        System.out.println("ENTER NEW TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
