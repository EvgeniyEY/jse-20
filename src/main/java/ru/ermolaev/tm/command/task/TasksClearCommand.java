package ru.ermolaev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;

public final class TasksClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        serviceLocator.getTaskService().removeAllTasks(userId);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
