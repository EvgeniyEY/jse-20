package ru.ermolaev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public final class TaskListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[TASK LIST]");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().showAllTasks(userId);
        for (@NotNull final Task task: tasks) {
            System.out.println((tasks.indexOf(task) + 1) + ". " + task);
        }
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
