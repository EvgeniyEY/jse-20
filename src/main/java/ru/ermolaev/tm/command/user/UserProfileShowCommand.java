package ru.ermolaev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.User;

public final class UserProfileShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-show-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about user account.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        @Nullable final User user = serviceLocator.getAuthenticationService().findCurrentUser();
        System.out.println(user);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
