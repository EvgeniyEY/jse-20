package ru.ermolaev.tm.command.data.json.fasterxml;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJsonFasterXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-json-fx-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json (fasterXML) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON (FASTERXML) LOAD]");
        @NotNull final String jsonData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON_FX)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(jsonData, Domain.class);

        serviceLocator.getDomainService().load(domain);

        System.out.println("[OK]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
