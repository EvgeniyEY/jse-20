package ru.ermolaev.tm.command.data.xml.jaxb;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public final class DataXmlJaxbSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-jb-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to XML (Jax-B) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML (JAX-B) SAVE]");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        @NotNull final File file = new File(DataConstant.FILE_XML_JB);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(domain, file);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
