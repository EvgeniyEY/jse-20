package ru.ermolaev.tm.command.data.xml.fasterxml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import ru.ermolaev.tm.enumeration.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlFasterXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-fx-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from XML (fasterXML) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML (FASTERXML) LOAD]");
        @NotNull final String xmlData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML_FX)));
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(xmlData, Domain.class);

        serviceLocator.getDomainService().load(domain);

        System.out.println("[OK]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
