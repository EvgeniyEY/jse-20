package ru.ermolaev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable Integer index = TerminalUtil.nextNumber();
        if (index == null) return;
        index = index - 1;
        serviceLocator.getProjectService().removeProjectByIndex(userId, index);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
