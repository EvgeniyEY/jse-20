package ru.ermolaev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String userId = serviceLocator.getAuthenticationService().getUserId();
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().findProjectByName(userId, name);
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
