package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    public E add(@NotNull final E e) {
        entities.add(e);
        return e;
    }

    public void add(@NotNull final List<E> es) {
        for (@NotNull final E e: es) add(e);
    }

    public void add(@NotNull final E... es) {
        for (@NotNull final E e: es) add(e);
    }

    public void clear() {
        entities.clear();
    }

    public void load(@NotNull final List<E> e) {
        clear();
        add(e);
    }

    public void load(@NotNull final E... e) {
        clear();
        add(e);
    }

}
