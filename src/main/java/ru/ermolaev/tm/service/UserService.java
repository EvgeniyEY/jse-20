package ru.ermolaev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.removeByEmail(email);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.hidePassword(password);
        if (saltPass == null) return null;
        user.setPasswordHash(saltPass);
        return userRepository.add(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updatePassword(@Nullable final String userId, @Nullable final String newPassword) {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        @Nullable final String saltPass = HashUtil.hidePassword(newPassword);
        if (saltPass == null) return null;
        user.setPasswordHash(saltPass);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserFirstName(@Nullable final String userId, @Nullable final String newFirstName) {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setFirstName(newFirstName);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserMiddleName(@Nullable final String userId, @Nullable final String newMiddleName) {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setMiddleName(newMiddleName);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserLastName(@Nullable final String userId, @Nullable final String newLastName) {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setLastName(newLastName);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserEmail(@Nullable final String userId, @Nullable final String newEmail) {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setEmail(newEmail);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

}
