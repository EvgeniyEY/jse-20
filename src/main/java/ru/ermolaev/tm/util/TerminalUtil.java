package ru.ermolaev.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.exception.incorrect.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @Nullable
    @SneakyThrows
    static Integer nextNumber() {
        @Nullable final String value = nextLine();
        if (value == null) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }
    }

}
