package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Error! Command is empty.");
    }

}
