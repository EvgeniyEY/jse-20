package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAllTasks(@NotNull String userId);

    @NotNull
    Task findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task removeByName(@NotNull String userId, @NotNull String name);

}
