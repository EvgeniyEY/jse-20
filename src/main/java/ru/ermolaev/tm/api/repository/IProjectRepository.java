package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAllProjects(@NotNull String userId);

    @NotNull
    Project findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeByName(@NotNull String userId, @NotNull String name);

}
