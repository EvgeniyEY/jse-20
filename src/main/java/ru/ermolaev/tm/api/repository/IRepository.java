package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository <E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E add(@NotNull E e);

    void add(@NotNull List<E> e);

    void add(@NotNull E... e);

    void clear();

    void load(@NotNull List<E> e);

    void load(@NotNull E... e);

}
