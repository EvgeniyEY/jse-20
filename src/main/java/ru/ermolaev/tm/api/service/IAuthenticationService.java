package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.entity.User;

public interface IAuthenticationService {

    @NotNull
    String getUserId();

    boolean isAuthenticated();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void registration(@Nullable String login, @Nullable String password, @Nullable String email);

    void updatePassword(@Nullable String newPassword);

    @Nullable
    User findCurrentUser();

    void updateUserFirstName(@Nullable String newFirstName);

    void updateUserMiddleName(@Nullable String newMiddleName);

    void updateUserLastName(@Nullable String newLastName);

    void updateUserEmail(@Nullable String newEmail);

    void checkRole(@Nullable Role[] roles);

}
