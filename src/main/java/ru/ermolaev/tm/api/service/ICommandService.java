package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    @NotNull
    List<AbstractCommand> getCommandList();

}
