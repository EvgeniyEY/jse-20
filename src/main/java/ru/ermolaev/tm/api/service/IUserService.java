package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User updatePassword(@Nullable String userId, @Nullable String newPassword);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeUser(@Nullable User user);

    @Nullable
    User removeById(@Nullable String id);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    @Nullable
    User updateUserFirstName(@Nullable String userId, @Nullable String newFirstName);

    @Nullable
    User updateUserMiddleName(@Nullable String userId, @Nullable String newMiddleName);

    @Nullable
    User updateUserLastName(@Nullable String userId, @Nullable String newLastName);

    @Nullable
    User updateUserEmail(@Nullable String userId, @Nullable String newEmail);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

}
